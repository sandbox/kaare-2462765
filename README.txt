
Organic groups cardinality
--------------------------------------------------------------------------------

Limit the number of group content nodes a group can have, either per group
member node type or per user.


Usage
--------------------------------------------------------------------------------

Setup content types as "Group content" under admin/structure/types/manage/%type
and set the limit and impact of limitation for this type. Tada!


Todo
--------------------------------------------------------------------------------

These are things that are nice/need to be done in order to make this a
full-blown project on d.o.

  * Allow global limits per group entity type
  * Allow limits per group. This might be designed in the same manner as OG
    manages field settings.

